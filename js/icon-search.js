/**
 * @file
 * JS file for Icon field widget.
 */
((Drupal, once) => {
  /**
   * Behavior for iconSearch.
   */
  Drupal.behaviors.iconSearch = {
    /**
     * Attach behaviour.
     */
    attach() {
      const searchField = once('icon-search', '#icon-search');
      if (!searchField.length) return;
      const icons = document.querySelectorAll('.icons-widget__radio');

      searchField[0].addEventListener('input', (e) => {
        const { value } = e.target;
        icons.forEach((icon) => {
          if (!icon.value.match(value)) {
            icon.parentElement.classList.add('hidden');
          } else {
            icon.parentElement.classList.remove('hidden');
          }
        });
      });
    },
  };
})(Drupal, once);
