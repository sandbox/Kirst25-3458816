<?php

namespace Drupal\icon_field\Plugin\Field\FieldWidget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Plugin implementation of the 'icon_select' widget.
 *
 * @FieldWidget(
 *   id = "icon_select",
 *   label = @Translation("Icon select"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class IconsFieldWidget extends WidgetBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The array of select options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The module handler used to find and execute the plugin hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory, RendererInterface $renderer, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->config = $config_factory->get('icon_field.settings');
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\WidgetPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('renderer'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#attached']['library'][] = 'icon_field/widget';
    $value = $items[$delta]->value ?: $items[0]->value;
    $options = $this->getOptions();

    $element['container'] = [
      '#type' => 'details',
      '#title' => $this->fieldDefinition->getLabel(),
      '#attributes' => [
        'class' => [
          'form-item',
          'icons-widget',
        ],
      ],
      '#required' => $element['#required'],
      "#description" => $element['#description'],
      "#field_parents" => $element['#field_parents'],
      '#weight' => $element['#weight'],
    ];

    $element['container']['search'] = [
      '#type' => 'search',
      '#title' => $this->t('Find icon:'),
      '#id' => 'icon-search',
    ];

    // Add an empty option if the widget needs one.
    if ($empty_label = $this->getEmptyLabel()) {
      $options = ['_none' => $empty_label] + $this->getOptions();
    }

    $element['container']['options'] = [
      '#default_value' => $value ?: '_none',
      '#type' => 'radios',
      '#options' => $options,
      '#required' => $element['#required'],
      '#id' => 'icons',
      '#attributes' => [
        'class' => [
          'icons-widget__radio',
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $data) {
      $values[$delta] = $data['container']['options'];
    }

    return $values;
  }

  /**
   * Returns the array of options for the widget.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions() {
    $this->options = [];
    $finder = new Finder();
    $directory = $this->config->get('directory') ?? $this->moduleHandler->getModule('icon_field')->getPath() . '/assets';
    $prefix = $this->config->get('icon_prefix') ?? NULL;

    if (is_dir($directory)) {
      $finder->files()->name($prefix . '*.svg')->in($directory);
      if ($finder->hasResults()) {
        foreach ($finder as $file) {
          $file_path = $file->getBasename('.svg');
          $view = [
            '#type' => 'inline_template',
            '#template' => '{{ svg|raw }}',
            '#context' => [
              'svg' => $file->getContents(),
            ],
          ];

          $this->options[$file_path] = $this->renderer->renderPlain($view);
        }
      }
    }

    return $this->options;
  }

  /**
   * Returns the empty option label to add to the list of options, if any.
   *
   * @return string|null
   *   Either a label of the empty option, or NULL.
   */
  protected function getEmptyLabel() {
    return $this->t('None');
  }

}
