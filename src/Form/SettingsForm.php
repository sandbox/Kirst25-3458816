<?php

namespace Drupal\icon_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure icon_field settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icon_field_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icon_field.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon directory'),
      '#default_value' => $this->config('icon_field.settings')->get('directory'),
      '#description' => $this->t('e.g. modules/custom/icons/assets'),
    ];
    $form['icon_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon file prefix'),
      '#default_value' => $this->config('icon_field.settings')->get('icon_prefix'),
      '#description' => $this->t('e.g. icon-'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('icon_field.settings')
      ->set('directory', $form_state->getValue('directory'))
      ->set('icon_prefix', $form_state->getValue('icon_prefix'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
