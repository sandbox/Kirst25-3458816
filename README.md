## INTRODUCTION

The Icon field module adds field widgets for selecting icons from a directory of svg files.

## CONFIGURATION
- Add a string (text) field - Note: This will be the value used to look for the file name.
- Configure the form display to use Icon widget.
- [Optional] Configure the manage display to use icon formatter to output as SVG.

Configure Icon settings (/admin/config/system/icons) to set a directory to search for the icons.
